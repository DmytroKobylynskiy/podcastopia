using System;
using System.ComponentModel.DataAnnotations;
using Domain.Feed.Events;
using Domain.SharedKernel.Model;
using Infrastructure.DDD;

namespace Domain.Feed.Model
{
    public class Item : Entity, IAggregateRoot{
        [Key]
        public int Id{get;set;}
        public int Feed_Id{get;set;}
        public string Link{get;set;}
        public string Guid{get;set;}
        public long Length { get; set; }
        public DateTime Pub_Date { get;set;}
        public string Type {get; set;}

        public Item(int feed_Id, string link, string guid, long length, DateTime pub_Date, string type)
        {
            Feed_Id = feed_Id;
            Link = link;
            Guid = guid;
            Length = length;
            Pub_Date = pub_Date;
            Type = type;
        }
    }
}
