using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Infrastructure.DDD;

namespace WebSite.Models
{
    public class Item : Entity, IAggregateRoot{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id{get;set;}
        public int Feed_Id{get;set;}
        public string Link{get;set;}
        public string Guid{get;set;}
        public long Length { get; set; }
        public DateTime Pub_Date { get;set;}
        public string Type {get; set;}

        protected Item(int id, int feed_Id, string link, string guid, long length, DateTime pub_Date, string type)
        {
            Feed_Id = feed_Id;
            Link = link;
            Guid = guid;
            Length = length;
            Pub_Date = pub_Date;
            Type = type;
        }

    }
}
