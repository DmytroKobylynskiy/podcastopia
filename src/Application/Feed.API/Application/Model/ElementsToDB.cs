using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feed.API.Application.Model
{
    public class ElementsToDB
    {
        public List<Item> items { get; set; }
        public List<Item_JSON> items_json { get; set; }
        public List<ModelFeed> feeds { get; set; }
    }
}