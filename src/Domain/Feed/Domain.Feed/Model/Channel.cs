﻿using System;
using System.Collections.Generic;
using Domain.Feed.Exceptions;
using Domain.SharedKernel.Model;
using Infrastructure.DDD;
// ReSharper disable UnusedMember.Local

namespace Domain.Feed.Model
{
    public class Channel : ValueObject
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        internal Channel(int _ChannelId, string  _Name, string _ImageUrl, bool _IsActive, bool _IsDeleted)
        {
            ChannelId = _ChannelId;
            Name = _Name;
            ImageUrl = _ImageUrl;
            IsActive = _IsActive;
            IsDeleted = _IsDeleted;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
