﻿using System;

namespace Channel.API.Application.Model
{
    public class CreateChannelCommand
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
