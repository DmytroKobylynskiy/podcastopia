﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebSite.Models
{
    public class ChannelRequestModel 
    {
        [Required]
        public string ImageUrl { get; set; }
        [Required]
        public string Name { get; set; }

    }
}
