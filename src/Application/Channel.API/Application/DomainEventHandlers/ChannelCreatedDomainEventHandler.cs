﻿using System;
using Domain.Channel.Events;
using System.Threading.Tasks;
using AutoMapper;
using Infrastructure.EventBus.Abstractions;
using Channel.API.Application.IntegrationEvents.Events;
using MediatR;

namespace Channel.API.Application.DomainEventHandlers
{
    public class ChannelCreatedDomainEventHandler : IAsyncNotificationHandler<ChannelCreatedDomainEvent>
    {
        private readonly IEventBus _eventBus;
        private readonly IMapper _mapper;

        public ChannelCreatedDomainEventHandler(IEventBus eventBus, IMapper mapper)
        {
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(ChannelCreatedDomainEvent notification)
        {
            // to update the query side (materialized view)
            var integrationEvent = _mapper.Map<ChannelCreatedIntegrationEvent>(notification);
            _eventBus.Publish(integrationEvent); // TODO: make an async Publish method.

            await Task.CompletedTask;
        }
    }
}
