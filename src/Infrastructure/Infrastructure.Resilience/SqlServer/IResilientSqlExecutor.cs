﻿namespace Infrastructure.Resilience.SqlServer
{
    public interface ISqlExecutor
    {
        ResilientExecutor<ISqlExecutor> CreateResilientSqlClient();
    }
}