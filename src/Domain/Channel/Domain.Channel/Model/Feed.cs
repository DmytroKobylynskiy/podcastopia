using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Channel.Model
{
    public class Feed
    {
        [Key]
        public int Feed_Id { get; set; }
        public string Feed_URL { get; set; }
        public string Feed_Description { get; set; }
        public string Name { get; set; }
        public string Channel_Name { get; set; }
        public int Channel_Id { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
