﻿using System;
using Domain.Feed.Model;
using Domain.SharedKernel.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Feed.Persistence
{
    /// <summary>
    /// This context is only to creates and runs the migrations. Just to example purposes and, avoids that you have to deal running scripts before to execute the solution.
    /// You must use <see cref="IFeedContext"/>
    /// </summary>
    [Obsolete("This context is only to creates and runs the migrations. Just to example purposes and, avoids that you have to deal running scripts before to execute the solution.")]
    public class FeedMigrationContext : DbContext
    {
        public FeedMigrationContext(DbContextOptions<FeedMigrationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new FeedEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ItemEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ItemJSONEntityTypeConfiguration());
        }

        public DbSet<ModelFeed> Feeds { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Item_JSON> Items_JSON { get; set; }
    }

    public class UserContextDesignFactory : IDesignTimeDbContextFactory<FeedMigrationContext>
    {
        public FeedMigrationContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<FeedMigrationContext>()
                .UseSqlServer("Server=.;Initial Catalog=feeddb;Integrated Security=true");

            return new FeedMigrationContext(optionsBuilder.Options);
        }
    }

    internal class FeedEntityTypeConfiguration : IEntityTypeConfiguration<ModelFeed>
    {
        public void Configure(EntityTypeBuilder<ModelFeed> builder)
        {
            builder.ToTable("Feeds");

            builder.HasKey(o => o.Feed_Id);

            builder.Property(x => x.Feed_Id)
                .IsRequired();

            builder.Ignore(b => b.DomainEvents);

            builder.Ignore(b => b.Id);

            builder.Property(x => x.Channel_Id)
                .IsRequired();

            builder.Property(x => x.Feed_Description)
                .IsRequired();

            builder.Property(x => x.IsDeleted)
                .IsRequired();

            builder.Property(x => x.IsActive)
                .IsRequired();

            builder.Property(x => x.Name)
                .IsRequired();

            builder.Property(x => x.Feed_URL)
                .IsRequired();
        }
    }

    internal class ItemEntityTypeConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.ToTable("Items");

            builder.HasKey(o => o.Id);
            
            builder.Ignore(b => b.DomainEvents);

            builder.Property(x => x.Feed_Id)
                .IsRequired();

            builder.Property(x => x.Guid)
                .IsRequired();

            builder.Property(x => x.Length)
                .IsRequired();

            builder.Property(x => x.Link)
                .IsRequired();

            builder.Property(x => x.Pub_Date)
                .IsRequired();

            builder.Property(x => x.Type)
                .IsRequired();
        }
    }

    internal class ItemJSONEntityTypeConfiguration : IEntityTypeConfiguration<Item_JSON>
    {
        public void Configure(EntityTypeBuilder<Item_JSON> builder)
        {
            builder.ToTable("Items_JSON");

            builder.HasKey(o => o.Json_id);

            builder.Property(x => x.Id)
                .IsRequired();

            builder.Property(x => x.Json_data)
                .IsRequired();
        }
    }
}
