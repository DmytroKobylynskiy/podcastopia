﻿using AutoMapper;
using Domain.Feed.Events;
using Feed.API.Application.IntegrationEvents.Events;
using Feed.API.Application.Model;

namespace Feed.API.Application.Mapping
{
    public class FeedEventsProfile : Profile
    {
        public FeedEventsProfile()
        {
            CreateMap<FeedCreatedDomainEvent, FeedCreatedIntegrationEvent>()
                .ConstructUsing(x => new FeedCreatedIntegrationEvent(
                    x.Feed_Id,
                    x.Feed_Description,
                    x.Feed_URL,
                    x.Name,
                    x.Channel_Id,
                    x.IsActive,
                    x.IsDeleted
                ));
        }
    }
}
