using System;

namespace Channel.API.Application.Model
{
    public class MediaOfChannelCommand
    {
        public Guid UserChannelId { get; set; }
        public string MediaOfChannel { get; set; }
    }
}
