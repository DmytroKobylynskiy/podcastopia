﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebSite.Models
{
    public class FeedRequestModel 
    {
        [Required]
        public string Feed_URL { get; set; }
        [Required]
        public int Channel_Id { get; set; }

    }
}
