﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Domain.Feed.Model
{
    public class ElementsToDB
    {
        public List<Item> items { get; set; }
        public List<Item_JSON> items_json { get; set; }
        public List<ModelFeed> feeds { get; set; }
    }
}