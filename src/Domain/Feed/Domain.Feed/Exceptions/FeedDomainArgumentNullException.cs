﻿using System;

namespace Domain.Feed.Exceptions
{
    public class FeedDomainArgumentNullException : ArgumentNullException
    {
        public FeedDomainArgumentNullException()
        { }

        public FeedDomainArgumentNullException(string message)
            : base(message)
        { }

        public FeedDomainArgumentNullException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
