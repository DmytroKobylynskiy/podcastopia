using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Xml.Linq;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Newtonsoft.Json;
using System.Globalization;
using Domain.Feed.Model;

namespace Feed.API.Infrastructure.Services
{

    public class HelperService
    {
        /*
            Parse items of feed. Returns items and items_json of feed 
         */
        public ElementsToDB ParseItems(int id,IEnumerable <XElement> items, IEnumerable<XElement> channel, List<XName> tags_list, JArray tags_json, int channelId)
        {
            try{
                var rss = new List<Item>();
                var _feeds = new List<ModelFeed>();
                List<Item_JSON> items_json = new List<Item_JSON>();
                int item_id = 0;
                foreach (var item in items)
                {
                    var str = item.ToString();
                    JArray tmp = new JArray();
                    item_id++;
                    for (int i = 0; i < tags_json.Count(); i++)
                    {
                        if (item.Element(tags_list[i]) != null && item.Element(tags_list[i]).ToString() != "")
                        {
                            tmp.Add(new JObject(new JProperty(tags_json[i].ToString(), 
                                SpecialCharactersHandler((string)item.Element(tags_list[i])))));
                        }
                    }
                    var json = tmp.ToString().Replace("'","''");
                    items_json.Add(new Item_JSON(item_id,json));
                    var time = (string)item.Element("pubDate");
                    if(item.Element("enclosure")!=null){
                        var type = (string)item.Element("enclosure").Attribute("type");
                        type = type.Substring(0,type.IndexOf("/"));
                        rss.Add(new Item(id,
                            (string)item.Element("enclosure").Attribute("url"),
                            SpecialCharactersHandler((string)item.Element("guid")),
                            (long)item.Element("enclosure").Attribute("length"),
                            TimeConverter(time),
                            type
                        ));
                    }
                }
                return new ElementsToDB()
                {
                    items = rss,
                    items_json = items_json,
                    feeds = ToFeeds(channel,channelId)
                };
            }catch(Exception ex){
                Console.WriteLine("Error message:"+ex.Message);
                return null;
            }    
        }

        public List<ModelFeed> ToFeeds(IEnumerable<XElement> channel,int channelId){
            var _feeds = new List<ModelFeed>(); 
            foreach(var feed in channel)
            {
                _feeds.Add(new ModelFeed(
                    SpecialCharactersHandler((string)feed.Element("link")),
                    (string)feed.Element("description"),
                    (string)feed.Element("title"),
                    channelId,
                    false,
                    true
                ));
                
            }
            return _feeds;
        }

        /*
            Remove special characters from item
         */
        public string SpecialCharactersHandler(string item)
        {
            char[] arr = item.ToCharArray();
            arr = Array.FindAll<char>(arr, (c => (char.IsLetterOrDigit(c)
                                              || char.IsWhiteSpace(c)
                                              || c == '-'
                                              || c == ':'
                                              || c == '/'
                                              || char.IsPunctuation(c))));
            return new string(arr);
        }

        /*
            Converts time format into correct timezone type
        */
        public DateTime TimeConverter(string input){
            String temp = input;
            //  Correct the timezone part
            for (int i = 0; i < Values.timeZones.Length; i++)
            {
                String timeZonePattern = @"\b" + Values.timeZones[i] + @"\b";
                String timeZoneReplPattern = String.Format("{0}:00", Values.utcOffsets[i].ToString("+00;-00"));
                temp = Regex.Replace(input, timeZonePattern, timeZoneReplPattern);
                if (temp != input)
                {
                    break;
                }
            }
            //  Correct the date part
            temp = Regex.Replace(temp, Values.datePattern, Values.dateReplPattern);
            DateTime dt = new DateTime();
            try
            {
                dt = DateTime.Parse(temp);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dt.ToUniversalTime();;
        }
        
        /*
            Encode to jwt list of items from getting feeds
         */
        public string ToJwt(List<Item[]> list)
        {
            var payload = new Dictionary<string, object>();
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            foreach (var item in list)
            {
                foreach(Item i in item)
                    payload.Add(i.Id.ToString(), i);
            }
            var token = encoder.Encode(payload, Values.secret);
            return token;
        }

        /*
            Encode to jwt common object for response
         */
        public string ToJwtResponse(object item)
        {
            var payload = new Dictionary<string, object>();
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            payload.Add("Response", item);
            var token = encoder.Encode(payload, Values.secret);
            return token;
        }

        /*
            Encode to jwt common object for response
        */
        public string TokenEncoderPayload(Dictionary<string,object> payload){
           
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            var token = encoder.Encode(payload, Values.secret);
            return token;
        }
        public string FromJWT(string token)
        {
            var json = "";
            try
            {
                IJsonSerializer serializer = new JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);
                json = decoder.Decode(token, Values.secret, verify: true);
                //Console.WriteLine(json);
            }
            catch (TokenExpiredException)
            {
                Console.WriteLine("Token has expired");
            }
            catch (SignatureVerificationException)
            {
                Console.WriteLine("Token has invalid signature");
            }
            return json;
        }
    }
}