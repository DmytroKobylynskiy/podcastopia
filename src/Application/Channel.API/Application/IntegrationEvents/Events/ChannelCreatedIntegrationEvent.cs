﻿using System;
using Infrastructure.EventBus.Events;
using Channel.API.Application.Model;

namespace Channel.API.Application.IntegrationEvents.Events
{
    public class ChannelCreatedIntegrationEvent : IntegrationEvent
    {
        public ChannelCreatedIntegrationEvent(int _ChannelId, string _Name, string _ImageUrl, bool _IsActive, bool _IsDeleted)
        {
            ChannelId = _ChannelId;
            Name = _Name;
            ImageUrl = _ImageUrl;
            IsActive = _IsActive;
            IsDeleted = _IsDeleted;
        }

        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
