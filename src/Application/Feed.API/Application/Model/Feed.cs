﻿using System;

namespace Feed.API.Application.Model
{
    public class ModelFeed
    {
        public int Feed_Id {get; set;}
        public string Feed_URL{get; set;}
        
        public string Feed_Description{get; set;}

        public string Name{get; set;}
        
        public int Channel_Id{get; set;}

        public bool IsDeleted{get; set;}

        public bool IsActive{get; set;}
    }
}
