﻿using System;

namespace Channel.API.Application.Model
{
    public class UpdateChannelCommand
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
