using AutoMapper;
using Domain.Feed.Model;
using Domain.Feed.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Feed.API.Infrastructure.Services
{
    public class ParserService
    {

        private static HttpClient Client { get; } = new HttpClient();

        private readonly IMapper _mapper;
        private readonly IFeedRepository _feedRepository;
        
        public ParserService(IFeedRepository feedRepository, IMapper mapper)
        {
            _feedRepository = feedRepository ?? throw new ArgumentNullException(nameof(feedRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<ElementsToDB>> FeedOfChannel(int ChannelId){
            var helperService = new HelperService();
            var feedsId = await _feedRepository.FeedsIdOfChannel(ChannelId);
            int i = 0; 
            List<ElementsToDB> elementsToDB = new List<ElementsToDB>();
            foreach (var feed_item in feedsId)
            {
                var element = await _feedRepository.GetFeedAsync(feed_item.Feed_Id);
                var stream = await Client.GetStreamAsync(element.Feed_URL);
                var feed = XDocument.Load(stream);
                var items = feed.Descendants("item");
                var channel = feed.Descendants("channel");
                JArray tags_json = new JArray();
                List<XName> tags_list = new List<XName>();
                //Getting all tags of xml
                foreach (var name in feed.Root.DescendantNodes().OfType<XElement>().Select(x => x.Name).Distinct())
                {
                    if (name.ToString() != "link" && name.ToString() != "guid" &&
                        name.ToString() != "pubDate" && name.ToString() != "item")
                    {
                        tags_list.Add(name);
                        tags_json.Add(name.ToString());
                    }
                }
                //Parsing elemets data
                elementsToDB.Add(
                    helperService.ParseItems(element.Feed_Id, items, channel, tags_list, tags_json, ChannelId)
                ) ;
                await _feedRepository.AddItemsAsync(elementsToDB.ElementAt(i).items);
                await _feedRepository.AddItemsJson(elementsToDB.ElementAt(i).items_json);
                i++;
            }
            return elementsToDB;
        }
    }
}
