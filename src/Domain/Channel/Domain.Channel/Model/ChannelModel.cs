﻿using System;
using Domain.Channel.Events;
using Infrastructure.DDD;
// ReSharper disable ConvertToAutoProperty
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedMember.Local

namespace Domain.Channel.Model
{
    public class ChannelModel : Entity, IAggregateRoot
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public ChannelModel()
        {
        }

        public ChannelModel(string _Name, string _ImageUrl, bool _IsActive, bool _IsDeleted)
        {
            Name = _Name;
            ImageUrl = _ImageUrl;
            IsActive = _IsActive;
            IsDeleted = _IsDeleted;

            AddDomainEvent(new ChannelCreatedDomainEvent( _Name, _ImageUrl, _IsActive, _IsDeleted));
        }
    }
}
