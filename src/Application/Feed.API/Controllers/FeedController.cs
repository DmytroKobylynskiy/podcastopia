﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Feed.Model;
using Domain.Feed.Repository;
using Domain.SharedKernel.Model;
using Feed.API.Infrastructure.Services;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using ViewModel = Feed.API.Application.Model;
using System.Net.Http;
using System.Xml.Linq;
using Infrastructure.Resilience.Http;
using RestSharp;
using Newtonsoft.Json;

namespace Feed.API.Controllers
{
    [Route("api/v1/[controller]")]
    public class FeedController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IFeedRepository _feedRepository;
        private ParserService parser;

        private readonly ResilientHttpInvoker _httpInvoker;
        private static HttpClient Client { get; } = new HttpClient();
        public FeedController(IFeedRepository feedRepository, IMapper mapper,
            ResilientHttpInvoker httpInvoker)
        {
            _feedRepository = feedRepository ?? throw new ArgumentNullException(nameof(feedRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            parser = new ParserService(feedRepository, mapper);
            _httpInvoker = httpInvoker;
        }

        /// <summary>
        /// Returns an feed that matches with the specified id
        /// </summary>
        /// <param name="feedId"></param>
        /// <returns>Returns an feed that matches with the specified id</returns>
        /// <response code="200">Returns an Feed object that matches with the specified id</response>
        [Route("getbyid")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetFeed(int feedId)
        {
            try
            {
                var feed = await _feedRepository.GetFeedAsync(feedId);
                return Ok(feed);
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }

        /// <summary>
        /// Returns an feed that matches with the specified Channel id
        /// </summary>
        /// <param name="ChannelId"></param>
        /// <returns>Returns an feed that matches with the specified Channel id</returns>
        /// <response code="200">Returns an feed that matches with the specified Channel id</response>
        [Route("getbyChannelid")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetFeedByChannel(int ChannelId)
        {
            try
            {
                var feed = await _feedRepository.GetFeedByChannelAsync(ChannelId);
                return Ok(feed);
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }

        /// <summary>
        /// Returns all of the Feeds
        /// </summary>
        /// <returns>Returns all of the Feeds</returns>
        /// <response code="200">Returns a list of Feed object.</response>
        [Route("get")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IEnumerable<ModelFeed>> GetFeeds()
        {
            try
            {
                var feeds = await _feedRepository.GetFeedsAsync();
                //var feedsViewModel = _mapper.Map<IEnumerable<ViewModel.ModelFeed>>(feeds);

                /*if (feedsViewModel == null)
                    return NotFound();*/
                return feeds;
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }

        /// <summary>
        /// Creates a new feed.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns the newly created feed identifier.</returns>
        /// <response code="201">Returns the message about creating new feed</response>
        [Route("create")]
        [HttpPost]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateFeed([FromBody]ViewModel.CreateFeedRequest request)
        {
            try
            {
                var helperService = new HelperService();
                var stream = await Client.GetStreamAsync(request.Feed_URL);
                var rss = XDocument.Load(stream);
                var channel = rss.Descendants("channel");
                var feeds = new List<ModelFeed>();
                string _feed_Description = "";
                string name = "";
                foreach (var f in channel)
                {
                    _feed_Description = (string)f.Element("description");
                    _feed_Description = _feed_Description.Replace("'","''");
                    name = (string)f.Element("title");
                }
                var createRequestRresponse = await _httpInvoker.InvokeAsync(async () =>
                {
                    var client = new RestClient("http://localhost:32778");
                    var _request = new RestRequest("/api/v1/Channel/getbyid?ChannelId="+request.Channel_Id, Method.GET);
                    //throw new InvalidOperationException("There was an error with Channel service");
                    return await client.ExecuteTaskAsync(_request);
                });
                if(JsonConvert.DeserializeObject<string>(createRequestRresponse.Content)!="Channel does not exist"){
                    var response = await _feedRepository.AddFeedAsync(new ModelFeed(request.Feed_URL,
                        _feed_Description,
                        name,
                        request.Channel_Id,
                        false,
                        true
                    ));
                    if(response!=0){
                    //throw new InvalidOperationException("There was an error with feed service "+createRequestRresponse.Content);
                        return Created(HttpContext.Request.GetUri().AbsoluteUri, "Feed succesfully created");
                    }else{
                        return Created(HttpContext.Request.GetUri().AbsoluteUri, "Feed is already exists");
                    }
                }else return Created(HttpContext.Request.GetUri().AbsoluteUri, "Channel does not exists");
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }

        /*/// <summary>
        /// Returns feeds of channel.
        /// </summary>
        /// <param name="ChannelId"></param>
        /// <returns>Returns the newly created feed identifier.</returns>
        /// <response code="201">Returns the newly created Channel identifier.</response>
        [Route("feeds")]
        [HttpGet]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Feeds(int ChannelId)
        {
            try
            {
                await parser.FeedOfChannel(ChannelId);
                return Created(HttpContext.Request.GetUri().AbsoluteUri, ChannelId);
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }*/


        /// <summary>
        /// Returns feeds of channel.
        /// </summary>
        /// <param name="ChannelId"></param>
        /// <returns>Returns the media of Channel.</returns>
        /// <response code="201">Returns the media of Channel .</response>
        [Route("media")]
        [HttpGet]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> MediaOfChannel(int ChannelId)
        {
            try
            {
                //var feeds = _feedRepository.GetFeedByChannelAsync(ChannelId);
                return Ok(await parser.FeedOfChannel(ChannelId));
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }

        /// <summary>
        /// Delete feed from channel.
        /// </summary>
        /// <param name="FeedId"></param>
        /// <returns>Returns id of Channel.</returns>
        /// <response code="201">Returns the media of Channel .</response>
        [Route("delete")]
        [HttpGet]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> DeleteFeed(int FeedId)
        {
            try
            {
                return Ok(await _feedRepository.DeleteFeedAsync(FeedId));
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }

        /// <summary>
        /// Update feed of channel.
        /// </summary>
        /// <param name="feed"></param>
        /// <returns>Returns the updated feed of Channel</returns>
        /// <response code="201">Returns the updated feed of Channel .</response>
        [Route("update")]
        [HttpPost]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateFeed([FromBody]ViewModel.CreateFeedRequest feed)
        {
            try
            {
                return Ok(await _feedRepository.UpdateFeedAsync(new ModelFeed{
                    Feed_Id = feed.Feed_Id,
                    Channel_Id = feed.Channel_Id,
                    Feed_Description = feed.Feed_Description,
                    Feed_URL = feed.Feed_URL
                }));
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }

        /// <summary>
        /// Update feed of channel.
        /// </summary>
        /// <param name="feedId"></param>
        /// <param name="channelId"></param>
        /// <returns>Returns the updated feed of Channel</returns>
        /// <response code="201">Returns the updated feed of Channel .</response>
        [Route("update")]
        [HttpPost]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateChannelIdOfFeed(int feedId, int channelId)
        {
            try
            {
                return Ok(await _feedRepository.UpdateChannelIdOfFeedAsync(feedId, channelId));
            }
            finally
            {
                _feedRepository.Dispose();
            }
        }
    }
}