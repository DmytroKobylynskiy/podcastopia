using System;
using Domain.Feed.Events;
using Domain.SharedKernel.Model;
using Infrastructure.DDD;

namespace Feed.API.Application.Model
{
    public class Item : Entity, IAggregateRoot{
        
        public int _id{get;set;}
        public int _feed_Id{get;set;}
        public string _link{get;set;}
        public string _guid{get;set;}
        public long _length { get; set; }
        public DateTime _pub_Date { get;set;}
        public string _type {get; set;}

        public Item(int feed_Id, string link, string guid, long length, DateTime pub_Date, string type)
        {
            _feed_Id = feed_Id;
            _link = link;
            _guid = guid;
            _length = length;
            _pub_Date = pub_Date;
            _type = type;
        }

    }
}
