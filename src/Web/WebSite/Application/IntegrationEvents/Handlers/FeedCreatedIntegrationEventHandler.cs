﻿using System;
using System.Threading.Tasks;
using Infrastructure.EventBus.Abstractions;
using WebSite.Application.IntegrationEvents.Events;
using WebSite.Infrastructure.Repository;
using WebSite.Models;

namespace WebSite.Application.IntegrationEvents.Handlers
{
    public class FeedCreatedIntegrationEventHandler: IIntegrationEventHandler<FeedCreatedIntegrationEvent>
    {
        private readonly IReportingRepository _reportingRepository;

        public FeedCreatedIntegrationEventHandler(IReportingRepository reportingRepository)
        {
            _reportingRepository = reportingRepository ?? throw new ArgumentNullException(nameof(reportingRepository));
        }

        public async Task Handle(FeedCreatedIntegrationEvent @event)
        {
            var feed = new Feed();

            // we throw an exception in order to don't send the Acknowledgement to the service bus, probably the consumer read the 
            // this message before that the created one.
            /*if (feed == null)
                throw new InvalidOperationException($"The Feed {@event.Feed_Id} doesn't exist. Error trying to update the materialized view.");
*/
            feed.Feed_Id = @event.Feed_Id;
            feed.Feed_URL = @event.Feed_URL;
            feed.Feed_Description = @event.Feed_Description;
            feed.Channel_Id = @event.Channel_Id;
            feed.Name = @event.Name;

            try
            {
                await _reportingRepository.AddFeedAsync(feed);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Error trying to create the Feed: {@event.Feed_Id},{@event.Feed_URL},{@event.Channel_Id}", ex);
            }
            finally
            {
                _reportingRepository.Dispose();
            }
        }
    }
}
