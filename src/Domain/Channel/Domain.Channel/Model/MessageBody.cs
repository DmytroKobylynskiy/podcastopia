using Infrastructure.DDD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Channel.Model
{
    public class MessageBody : IAggregateRoot
    {
        private Guid aggregateRootId;
        private int userChannelId;
        private string mediaOfChannel;

        public MessageBody(Guid aggregateRootId, int userChannelId, string mediaOfChannel)
        {
            this.aggregateRootId = aggregateRootId;
            this.userChannelId = userChannelId;
            this.mediaOfChannel = mediaOfChannel;
        }

        [Key]
        public int Channel_Id { get; set; }
        public string Feed_URL { get; set; }
        public string Feed_Description { get; set; }
        public string Name { get; set; }
        public string Channel_Name { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
