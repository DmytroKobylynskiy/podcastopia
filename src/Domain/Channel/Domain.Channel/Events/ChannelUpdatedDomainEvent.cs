﻿using System;
using Domain.SharedKernel.Model;
using Domain.Channel.Model;
using MediatR;

namespace Domain.Channel.Events
{
    public class ChannelUpdatedDomainEvent : INotification
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
