using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Domain.User.Model;
using Domain.User.Repository;
using Infrastructure.Resilience.Http;
using WebSite.Extensions;
using WebSite.Hubs;
using WebSite.Infrastructure.Repository;
using WebSite.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
// ReSharper disable ForCanBeConvertedToForeach

namespace WebSite.Controllers
{
    public class FeedController : Controller
    {
        private readonly IMemoryCache _cache;
        private readonly IUserRepository _userRepository;
        private readonly ResilientHttpInvoker _httpInvoker;
        private readonly IHubContext<ChannelHub> _hubContext;

        private readonly IHubContext<ChannelHub> _channelHubContext;

        private readonly IHubContext<FeedHub> _feedHubContext;
        private readonly IReportingRepository _reportingRepository;
        private readonly IOptions<ChannelApiSettings> _ChannelApiSettings;

        private readonly IOptions<FeedApiSettings> _FeedApiSettings;
        public FeedController(IUserRepository userRepository,
            IMemoryCache cache,
            ResilientHttpInvoker httpInvoker,
            IOptions<ChannelApiSettings> ChannelApiSettings,
            IOptions<FeedApiSettings> FeedApiSettings,
            IHubContext<ChannelHub> channelHubContext,
            IHubContext<FeedHub> feedHubContext,
            IReportingRepository reportingRepository)
        {
            _userRepository = userRepository;
            _cache = cache;
            _httpInvoker = httpInvoker;
            _ChannelApiSettings = ChannelApiSettings;
            _FeedApiSettings = FeedApiSettings;
            _channelHubContext = channelHubContext;
            _feedHubContext = feedHubContext;
            _reportingRepository = reportingRepository;
        }

        public async Task<IActionResult> Index()
        {

            return View();
        }

        public async Task<IActionResult> Feeds()
        {
            return View(await GetFeeds());
        }

        [HttpPost]
        public async Task<IActionResult> CreateFeedAsync(FeedRequestModel feedRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.AllErrors());

            await _feedHubContext.Clients.All.SendAsync("Feed", "Created"); 

            await CreateFeed(feedRequest);

            return Ok();
        }

        public async Task<IActionResult> ChannelsByUser()
        {
            var users = await GetUsers();
            var usersModel = users.Select(x => new UserModel
            {
                Id = x.Id,
                Email = x.Email,
                Name = x.Name,
                NumberPhone = x.NumberPhone
            });

            return View(usersModel.ToList());
        }

        public async Task<IActionResult> ChannelById(int id)
        {
            try
            {
                var Channel = await _reportingRepository.GetChannelAsync(id);
                return View("ChannelDetails", Channel);
            }
            finally
            {
                _reportingRepository.Dispose();
            }
        }

        private async Task<IList<User>> GetUsers()
        {
            var users = await GetDataFromCache("users", () => _userRepository.GetUsersAsync());
            return users;
        }

        private async Task<T> GetDataFromCache<T>(string cacheKey, Func<Task<T>> action)
        {
            if (!_cache.TryGetValue(cacheKey, out T result))
            {
                result = await action();

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromMinutes(5));

                _cache.Set(cacheKey, result, cacheEntryOptions);
            }

            return result;
        }

        private async Task<int> CreateFeed(FeedRequestModel feedRequest)
        {
            var createRequestRresponse = await _httpInvoker.InvokeAsync(async () =>
            {
                var client = new RestClient(_FeedApiSettings.Value.BaseUrl);
                var request = new RestRequest(_FeedApiSettings.Value.CreateUrl, Method.POST);
                request.AddJsonBody(new
                {
                    Feed_URL = feedRequest.Feed_URL,
                    Channel_Id = feedRequest.Channel_Id
                });
                //throw new InvalidOperationException("There was an error with Channel service");
                return await client.ExecuteTaskAsync(request);
            });

            /*if (createRequestRresponse.StatusCode != HttpStatusCode.Created)
                throw new InvalidOperationException("There was an error with Channel service", createRequestRresponse.ErrorException);
                */
            return JsonConvert.DeserializeObject<int>(createRequestRresponse.Content);
        }

        private async Task<IEnumerable<Feed>> GetFeeds(){

            var createRequestRresponse = await _httpInvoker.InvokeAsync(async () =>
            {
                var client = new RestClient(_FeedApiSettings.Value.BaseUrl);
                var request = new RestRequest("/api/v1/Feed/get", Method.GET);

                //throw new InvalidOperationException("There was an error with Channel service");
                return await client.ExecuteTaskAsync(request);
            });

            /*if (createRequestRresponse.StatusCode != HttpStatusCode.Created)
                throw new InvalidOperationException("There was an error with Channel service"+createRequestRresponse.Content);
                */
            return JsonConvert.DeserializeObject<IEnumerable<Feed>>(createRequestRresponse.Content);
        }

        private async Task AcceptOrStartChannel(string action, int ChannelId)
        {
            var createRequestRresponse = await _httpInvoker.InvokeAsync(async () =>
            {
                var client = new RestClient(_ChannelApiSettings.Value.BaseUrl);
                var request = new RestRequest(action, Method.PUT);
                request.AddJsonBody(new { id = ChannelId.ToString() });

                return await client.ExecuteTaskAsync(request);
            });

            if (createRequestRresponse.StatusCode != HttpStatusCode.OK)
                throw new InvalidOperationException("There was an error with Channel service", createRequestRresponse.ErrorException);
        }
    }
}