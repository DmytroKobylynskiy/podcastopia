﻿using System;
using Domain.Feed.Events;
using Domain.SharedKernel.Model;
using Infrastructure.DDD;

namespace Domain.Feed.Model
{
    public class ModelFeed : Entity, IAggregateRoot
    {
        public int Feed_Id { get; set; }
        public string Feed_URL { get; set; }

        public string Feed_Description { get; set; }

        public string Name { get; set; }

        public int Channel_Id { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }
        public int _FeedId => Feed_Id;
        public string _Feed_URL => Feed_URL;

        public string _Feed_Description => Feed_Description;

        public int _Channel_Id => Channel_Id;
        
        public string _Name => Name;

        public bool _IsDeleted => IsDeleted;
        public bool _IsActive => IsActive;

        // to Dapper mapping.
        public ModelFeed(int feedId, string feed_URL, string feed_Description, string name, int channelId, bool isDeleted, bool isActive)
        {
            Feed_Id = feedId;
            Feed_URL = feed_URL;
            Feed_Description = feed_Description;
            Name = name;
            Channel_Id = channelId;
            IsActive = isActive;
            IsDeleted = isDeleted;
        }

        public ModelFeed()
        {

        }

        public ModelFeed( string feed_URL, string feed_Description, string name, int channelId, bool isDeleted, bool isActive)
        {
            Feed_URL = feed_URL;
            Feed_Description = feed_Description;
            Name = name;
            Channel_Id = channelId;
            IsActive = isActive;
            IsDeleted = isDeleted;

            AddDomainEvent(new FeedCreatedDomainEvent(Feed_Id, Feed_URL, Feed_Description, Name, Channel_Id, IsDeleted, IsActive));
        }
        
    }
}
