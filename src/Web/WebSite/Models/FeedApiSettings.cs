﻿namespace WebSite.Models
{
    public class FeedApiSettings
    {
        public string BaseUrl { get; set; }

        public string CreateUrl { get; set; }

        public string FeedsUrl { get; set; }

        public string AcceptUrl { get; set; }

        public string StartUrl { get; set; }

        public string CancelUrl { get; set; }

        public string UpdateCurrentLocationUrl { get; set; }
    }
}
