﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Channel.Model;
using Domain.Channel.Persistence;
using Infrastructure.DDD;

namespace Domain.Channel.Repository
{ 
    public class ChannelRepository : IChannelRepository
    {
        private readonly IChannelContext _context;

        public ChannelRepository(IChannelContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<ChannelModel> GetChannelAsync(int id)
        {
            return await _context.QuerySingleAsync<ChannelModel>("Select * from Channels where ChannelId = @id", new { id = id });
        }

        public async Task<ChannelModel> GetChannelByIdAsync(int _Channel_Id)
        {
            return await _context.QuerySingleAsync<ChannelModel>(
                "Select * " +
                "From Channels  " +
                "Where ChannelId = @Channel_Id",
                new { Channel_Id = _Channel_Id });
        }

        public async Task<IEnumerable<ChannelModel>> GetChannelsAsync()
        {
            return await _context.QueryAsync<ChannelModel>(
                "Select * " +
                "From Channels");
        }

        public async Task<int> AddChannelAsync(ChannelModel Channel)
        {
            return await _context.ExecuteAsync(
                Channel,
                "Insert Into Channels(ImageUrl, Name, IsActive, IsDeleted) Values(@ImageUrl, @Name, @IsActive, @IsDeleted)",
                new
                {
                    Channel.ImageUrl,
                    Channel.Name,
                    Channel.IsActive,
                    Channel.IsDeleted
                });
        }

        public async Task<ChannelModel> DeleteChannelAsync(int id)
        {
            return await _context.QuerySingleAsync<ChannelModel>(
                "Update Channels set IsDeleted = @IsDeleted where ChannelId = @channelId",
                new
                {
                    channelId = id,
                    IsDeleted = true
                });
        }

        public async Task<ChannelModel> UpdateChannelNameAsync(int id, string name)
        {
            return await _context.QuerySingleAsync<ChannelModel>(
                "Update Channels set Name = @_name where ChannelId = @channelId",
                new
                {
                    channelId = id,
                    _name = name
                });
        }

        public async Task<ChannelModel> UpdateChannelImageAsync(int id, string imageUrl)
        {
            return await _context.QuerySingleAsync<ChannelModel>(
                "Update Channels set ImageUrl = @image where ChannelId = @channelId",
                new
                {
                    channelId = id,
                    image = imageUrl
                });
        }

        public async Task AddChannelsAsync(List<ChannelModel> Channels)
        {
            foreach(ChannelModel Channel in Channels){
                await _context.ExecuteAsync(
                Channel,
                "Insert Into Channels(ImageUrl, Name, IsActive, IsDeleted) Values(@ImageUrl, @Name, @IsActive, @IsDeleted)",
                new
                {
                    Channel.ImageUrl,
                    Channel.Name,
                    Channel.IsActive,
                    Channel.IsDeleted
                });
            }
        }
        

        public async Task<ChannelModel> GetChannelByNameAsync(string _name)
        {
            return await _context.QuerySingleAsync<ChannelModel>("Select * from Channels where Name = @name", new { name = _name });
        }
        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}