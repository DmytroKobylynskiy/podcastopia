﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Channel.Model;
using Domain.Channel.Repository;
using Domain.SharedKernel.Model;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using ViewModel = Channel.API.Application.Model;
using System.Net.Http;
using System.Xml.Linq;

namespace Channel.API.Controllers
{
    [Route("api/v1/[controller]")]
    public class ChannelController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IChannelRepository _ChannelRepository;
        private static HttpClient Client { get; } = new HttpClient();
        public ChannelController(IChannelRepository ChannelRepository, IMapper mapper)
        {
            _ChannelRepository = ChannelRepository ?? throw new ArgumentNullException(nameof(ChannelRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Returns an Channel that matches with the specified id
        /// </summary>
        /// <param name="ChannelId"></param>
        /// <returns>Returns an Channel that matches with the specified id</returns>
        /// <response code="200">Returns an Channel object that matches with the specified id</response>
        [Route("getbyid")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetChannel(int ChannelId)
        {
            try
            {
                var Channel = await _ChannelRepository.GetChannelAsync(ChannelId);
                if(Channel!=null){
                    return Ok(Channel);
                }else{
                    return Ok("Channel does not exist");
                }
                
            }
            finally
            {
                _ChannelRepository.Dispose();
            }
        }

        /// <summary>
        /// Returns all of the Channels
        /// </summary>
        /// <returns>Returns all of the Channels</returns>
        /// <response code="200">Returns a list of Channel object.</response>
        [Route("get")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetChannels()
        {
            try
            {
                var Channels = await _ChannelRepository.GetChannelsAsync();
                //var ChannelsViewModel = _mapper.Map<IEnumerable<ViewModel.ChannelModel>>(Channels);

                //if (ChannelsViewModel == null)
                  //  return NotFound();
                return Ok(Channels);
            }
            finally
            {
                _ChannelRepository.Dispose();
            }
        }

        /// <summary>
        /// Creates a new channel.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns the newly created channel identifier.</returns>
        /// <response code="201">Returns the newly created Channel identifier.</response>
        [Route("create")]
        [HttpPost]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateChannel([FromBody]ViewModel.CreateChannelCommand request)
        {
            try
            {
                //var feed = await _feedRepository.GetFeedByChannelAsync(request._channel_Id);
                //if (feed != null) return Created(HttpContext.Request.GetUri().AbsoluteUri, feed.FeedId);

                var channel = new ChannelModel(
                    request.Name,
                    request.ImageUrl,
                    request.IsActive,
                    request.IsDeleted
                );

                await _ChannelRepository.AddChannelAsync(new ChannelModel(request.Name,
                        request.ImageUrl,
                        true,
                        false
                    ));
                    //
                return Created(HttpContext.Request.GetUri().AbsoluteUri, "Channel succesfully " +request.Name+ " created");
            }
            finally
            {
                _ChannelRepository.Dispose();
            }
        }

        /// <summary>
        /// Delete a  channel.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the deleted channel identifier.</returns>
        /// <response code="201">Returns the newly created Channel identifier.</response>
        [Route("delete")]
        [HttpGet]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> DeleteChannelAsync(int id)
        {
            try
            {
                await _ChannelRepository.DeleteChannelAsync(id);
                //
                return Created(HttpContext.Request.GetUri().AbsoluteUri, 1);
            }
            finally
            {
                _ChannelRepository.Dispose();
            }
        }
        
        /// <summary>
        /// Update a name of channel.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns>Returns updated channel identifier.</returns>
        /// <response code="201">Returns updated Channel identifier.</response>
        [Route("updateName")]
        [HttpGet]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateChannelNameAsync(int id, string name)
        {
            try
            {
                await _ChannelRepository.UpdateChannelNameAsync(id,name);
                //
                return Created(HttpContext.Request.GetUri().AbsoluteUri, "Channel name succesfully updated on " +name);
            }
            finally
            {
                _ChannelRepository.Dispose();
            }
        }

        /// <summary>
        /// Update a image of channel.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="imageUrl"></param>
        /// <returns>Returns the updated channel identifier.</returns>
        /// <response code="201">Returns updated Channel identifier.</response>
        [Route("updateImage")]
        [HttpGet]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateChannelImageAsync(int id, string imageUrl)
        {
            try
            {
                //var feed = await _feedRepository.GetFeedByChannelAsync(request._channel_Id);
                //if (feed != null) return Created(HttpContext.Request.GetUri().AbsoluteUri, feed.FeedId);

                await _ChannelRepository.UpdateChannelImageAsync(id, imageUrl);
                //
                return Created(HttpContext.Request.GetUri().AbsoluteUri, "Channel image succesfully updated on " +imageUrl);
            }
            finally
            {
                _ChannelRepository.Dispose();
            }
        }
    }
}