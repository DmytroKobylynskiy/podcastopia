﻿using System;
using Domain.Feed.Events;
using System.Threading.Tasks;
using AutoMapper;
using Infrastructure.EventBus.Abstractions;
using Feed.API.Application.IntegrationEvents.Events;
using MediatR;

namespace Feed.API.Application.DomainEventHandlers
{
    public class FeedCreatedDomainEventHandler : IAsyncNotificationHandler<FeedCreatedDomainEvent>
    {
        private readonly IEventBus _eventBus;
        private readonly IMapper _mapper;

        public FeedCreatedDomainEventHandler(IEventBus eventBus, IMapper mapper)
        {
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(FeedCreatedDomainEvent notification)
        {
            // to update the query side (materialized view)
            var integrationEvent = _mapper.Map<FeedCreatedIntegrationEvent>(notification);
            _eventBus.Publish(integrationEvent); // TODO: make an async Publish method.

            await Task.CompletedTask;
        }
    }
}
