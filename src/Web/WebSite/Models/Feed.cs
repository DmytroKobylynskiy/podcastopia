using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Models
{
    public class Feed{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Feed_Id{get;set;}
        public string Feed_URL{get;set;}
        public string Feed_Description { get; set; }
        public string Name{get;set;}
        public int Channel_Id { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

    }
}