﻿using WebSite.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace WebSite.Infrastructure.Persistence
{
    public class ReportingContext : DbContext
    {
        // ReSharper disable once InconsistentNaming
        private const string DEFAULT_SCHEMA = "Reporting";

        public ReportingContext(DbContextOptions<ReportingContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Channel> Channels { get; set; }
        public DbSet<Feed> Feeds { get; set; }
    }

    public class ReportingContextDesignFactory : IDesignTimeDbContextFactory<ReportingContext>
    {
        public ReportingContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ReportingContext>()
                .UseSqlServer("Server=.;Initial Catalog=.WebSiteDb;Integrated Security=true");

            return new ReportingContext(optionsBuilder.Options);
        }
    }
}
