﻿using System.Threading.Tasks;
using Domain.SharedKernel.Model;

namespace Domain.ACL.Contracts
{
    public interface IPaymentServiceAdapter
    {
        Task<PaymentInfo> ProcessPaymentAsync(int userId, string reference);
    }
}