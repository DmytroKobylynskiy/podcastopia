﻿using Infrastructure.DDD;

namespace Infrastructure.Repository.Abstractions
{
    public interface IRepository<T> where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
