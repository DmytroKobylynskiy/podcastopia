﻿using Domain.Feed.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Feed.Repository
{
    public interface IFeedRepository : IDisposable
    {
        Task<ModelFeed> GetFeedAsync(int id);

        Task<ModelFeed> GetFeedByChannelAsync(int ChannelId);

        Task<IEnumerable<ModelFeed>> GetFeedsAsync();
        Task<IEnumerable<FeedId>> FeedsIdOfChannel(int id);

        Task<int> AddFeedAsync(ModelFeed feed);
        Task<ModelFeed> DeleteFeedAsync(int feed);
        Task<ModelFeed> UpdateFeedAsync(ModelFeed feed);
        Task<ModelFeed> UpdateChannelIdOfFeedAsync(int feedId, int channelId);
        Task AddFeedsAsync(List<ModelFeed> feeds);
        Task AddItemsAsync(List<Item> items);
        Task AddItemsJson(List<Item_JSON> items_json);
    }
}