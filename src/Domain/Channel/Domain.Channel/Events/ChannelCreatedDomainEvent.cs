﻿using Domain.SharedKernel.Model;
using Domain.Channel.Model;
using System;
using MediatR;

namespace Domain.Channel.Events
{
    public class ChannelCreatedDomainEvent : INotification
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public ChannelCreatedDomainEvent(string _Name, string _ImageUrl, bool _IsActive, bool _IsDeleted)
        {
            Name = _Name;
            ImageUrl = _ImageUrl;
            IsActive = _IsActive;
            IsDeleted = _IsDeleted;
        }
    }
}
