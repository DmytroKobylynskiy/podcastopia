﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Channel.Exceptions
{
    public class ChannelDomainInvalidOperationException : InvalidOperationException
    {
        public ChannelDomainInvalidOperationException()
        { }

        public ChannelDomainInvalidOperationException(string message)
            : base(message)
        { }

        public ChannelDomainInvalidOperationException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
