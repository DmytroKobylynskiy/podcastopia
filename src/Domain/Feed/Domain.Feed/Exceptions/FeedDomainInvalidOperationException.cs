﻿using System;

namespace Domain.Feed.Exceptions
{
    public class FeedDomainInvalidOperationException : InvalidOperationException
    {
        public FeedDomainInvalidOperationException()
        { }

        public FeedDomainInvalidOperationException(string message)
            : base(message)
        { }

        public FeedDomainInvalidOperationException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
