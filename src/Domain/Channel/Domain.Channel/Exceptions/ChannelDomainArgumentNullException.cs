﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Channel.Exceptions
{
    public class ChannelDomainArgumentNullException : ArgumentNullException
    {
        public ChannelDomainArgumentNullException()
        { }

        public ChannelDomainArgumentNullException(string message)
            : base(message)
        { }

        public ChannelDomainArgumentNullException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
