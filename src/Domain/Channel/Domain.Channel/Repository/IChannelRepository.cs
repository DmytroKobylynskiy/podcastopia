﻿
using Domain.Channel.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Channel.Repository
{
    public interface IChannelRepository : IDisposable
    {
        Task<ChannelModel> GetChannelAsync(int id);

        Task<IEnumerable<ChannelModel>> GetChannelsAsync();

        Task<int> AddChannelAsync(ChannelModel Channel);
        
        Task<ChannelModel> DeleteChannelAsync(int id);

        Task<ChannelModel> UpdateChannelNameAsync(int id, string name);

        Task<ChannelModel> UpdateChannelImageAsync(int id, string imageUrl);
    }
}