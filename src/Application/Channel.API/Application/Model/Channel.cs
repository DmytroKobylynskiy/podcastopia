﻿using System;
using System.Collections.Generic;
using Domain.Channel.Exceptions;
using Domain.SharedKernel.Model;
using Infrastructure.DDD;
// ReSharper disable UnusedMember.Local

namespace Channel.API.Application.Model
{
    public class ChannelModel : ValueObject
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        internal ChannelModel(string  _Name, string _ImageUrl, bool _IsActive, bool _IsDeleted)
        {
            Name = _Name;
            ImageUrl = _ImageUrl;
            IsActive = _IsActive;
            IsDeleted = _IsDeleted;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
