﻿using System;
using System.Threading.Tasks;
using Domain.User.Repository;
using Infrastructure.EventBus.Abstractions;
using WebSite.Application.IntegrationEvents.Events;
using WebSite.Infrastructure.Repository;
using WebSite.Models;

namespace WebSite.Application.IntegrationEvents.Handlers
{
    public class ChannelCreatedIntegrationEventHandler : IIntegrationEventHandler<ChannelCreatedIntegrationEvent>
    {
        private readonly IReportingRepository _reportingRepository;
        private readonly IUserRepository _userRepository;

        public ChannelCreatedIntegrationEventHandler(IReportingRepository reportingRepository, IUserRepository userRepository)
        {
            _reportingRepository = reportingRepository ?? throw new ArgumentNullException(nameof(reportingRepository));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task Handle(ChannelCreatedIntegrationEvent @event)
        {
            var existingChannel = _reportingRepository.GetChannel(@event.ChannelId);
            if (existingChannel != null) return;

            var newChannel = new Channel
            {
                ChannelId = @event.ChannelId,
                Name = @event.Name,
                ImageUrl = @event.ImageUrl,
                IsActive = @event.IsActive,
                IsDeleted = @event.IsDeleted
            };

            try
            {
                _reportingRepository.AddChannel(newChannel);
                await Task.CompletedTask;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Error trying to create the Channel: {@event.ChannelId}", ex);
            }
            finally
            {
                _reportingRepository.Dispose();
            }
        }
    }
}
