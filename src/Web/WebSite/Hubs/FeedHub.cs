using System.Threading.Tasks;
using WebSite.Models;
using Microsoft.AspNetCore.SignalR;

namespace WebSite.Hubs
{
    public class FeedHub : Hub
    {
        public Task UpdateFeedUrl(string feedUrl)
        {
            return Clients.All.SendAsync("UpdateFeed", feedUrl);
        }

        public Task UpdateName(string name)
        {
            return Clients.All.SendAsync("UpdateName", name);
        }

        public Task UpdateActive(bool isActive)
        {
            return Clients.All.SendAsync("NotifyFeed", isActive);
        }

        public Task UpdateDelete(bool isDelete)
        {
            return Clients.All.SendAsync("NotifyFeed", isDelete);
        }
    }
}
