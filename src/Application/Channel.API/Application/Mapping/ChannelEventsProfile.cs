﻿using AutoMapper;
using Domain.Channel.Events;
using Channel.API.Application.IntegrationEvents;
using Channel.API.Application.Model;
using Channel.API.Application.IntegrationEvents.Events;

namespace Channel.API.Application.Mapping
{
    public class ChannelEventsProfile : Profile
    {
        public ChannelEventsProfile()
        {
            CreateMap<ChannelCreatedDomainEvent, ChannelCreatedIntegrationEvent>()
                .ConstructUsing(x => new ChannelCreatedIntegrationEvent(
                    x.ChannelId,
                    x.Name,
                    x.ImageUrl,
                    x.IsActive,
                    x.IsDeleted
                ));
        }
    }
}
