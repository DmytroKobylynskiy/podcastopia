﻿using System.Threading.Tasks;
using WebSite.Models;
using Microsoft.AspNetCore.SignalR;

namespace WebSite.Hubs
{
    public class ChannelHub : Hub
    {
        public Task UpdateImage(string imageUrl)
        {
            return Clients.All.SendAsync("UpdateImage", imageUrl);
        }

        public Task UpdateName(string name)
        {
            return Clients.All.SendAsync("UpdateName", name);
        }

        public Task UpdateActive(bool isActive)
        {
            return Clients.All.SendAsync("NotifyChannel", isActive);
        }

        public Task UpdateDelete(bool isDelete)
        {
            return Clients.All.SendAsync("NotifyChannel", isDelete);
        }
    }
}
