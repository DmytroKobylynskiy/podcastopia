﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Domain.User.Model;
using Domain.User.Repository;
using Infrastructure.Resilience.Http;
using WebSite.Extensions;
using WebSite.Hubs;
using WebSite.Infrastructure.Repository;
using WebSite.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
// ReSharper disable ForCanBeConvertedToForeach

namespace WebSite.Controllers
{
    public class ChannelController : Controller
    {
        private readonly IMemoryCache _cache;
        private readonly IUserRepository _userRepository;
        private readonly ResilientHttpInvoker _httpInvoker;
        private readonly IHubContext<ChannelHub> _hubContext;

        private readonly IHubContext<ChannelHub> _channelHubContext;

        private readonly IHubContext<FeedHub> _feedHubContext;
        private readonly IReportingRepository _reportingRepository;
        private readonly IOptions<ChannelApiSettings> _ChannelApiSettings;

        private readonly IOptions<FeedApiSettings> _FeedApiSettings;
        public ChannelController(IUserRepository userRepository,
            IMemoryCache cache,
            ResilientHttpInvoker httpInvoker,
            IOptions<ChannelApiSettings> ChannelApiSettings,
            IOptions<FeedApiSettings> FeedApiSettings,
            IHubContext<ChannelHub> channelHubContext,
            IHubContext<FeedHub> feedHubContext,
            IReportingRepository reportingRepository)
        {
            _userRepository = userRepository;
            _cache = cache;
            _httpInvoker = httpInvoker;
            _ChannelApiSettings = ChannelApiSettings;
            _FeedApiSettings = FeedApiSettings;
            _channelHubContext = channelHubContext;
            _feedHubContext = feedHubContext;
            _reportingRepository = reportingRepository;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateChannelAsync(ChannelRequestModel channelRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.AllErrors());

            await _channelHubContext.Clients.All.SendAsync("NotifyChannel", "Created");

            await CreateChannel(channelRequest);

            //await CreateFeed();
            /*await _hubContext.Clients.All.SendAsync("NotifyChannel", "Accepted");

            await AcceptOrStartChannel(_ChannelApiSettings.Value.StartUrl , int.NewGuid());
            await _hubContext.Clients.All.SendAsync("NotifyChannel", "Started");

            await _hubContext.Clients.All.SendAsync("UpdateChannel", "123");

            await _hubContext.Clients.All.SendAsync("NotifyChannel", "Finished");*/

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> MediaOfChannelAsync()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.AllErrors());

            await _channelHubContext.Clients.All.SendAsync("NotifyChannel", "Created");

            //await CreateChannel();

            //await CreateFeed();
            /*await _hubContext.Clients.All.SendAsync("NotifyChannel", "Accepted");

            await AcceptOrStartChannel(_ChannelApiSettings.Value.StartUrl , int.NewGuid());
            await _hubContext.Clients.All.SendAsync("NotifyChannel", "Started");

            await _hubContext.Clients.All.SendAsync("UpdateChannel", "123");

            await _hubContext.Clients.All.SendAsync("NotifyChannel", "Finished");*/

            return Ok();
        }

        public async Task<IActionResult> ChannelsByUser()
        {
            var users = await GetUsers();
            var usersModel = users.Select(x => new UserModel
            {
                Id = x.Id,
                Email = x.Email,
                Name = x.Name,
                NumberPhone = x.NumberPhone
            });

            return View(usersModel.ToList());
        }

        public async Task<IActionResult> ChannelById(int id)
        {
            try
            {
                var Channel = await _reportingRepository.GetChannelAsync(id);
                return View("ChannelDetails", Channel);
            }
            finally
            {
                _reportingRepository.Dispose();
            }
        }

        private async Task<IList<User>> GetUsers()
        {
            var users = await GetDataFromCache("users", () => _userRepository.GetUsersAsync());
            return users;
        }

        private async Task<T> GetDataFromCache<T>(string cacheKey, Func<Task<T>> action)
        {
            if (!_cache.TryGetValue(cacheKey, out T result))
            {
                result = await action();

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromMinutes(5));

                _cache.Set(cacheKey, result, cacheEntryOptions);
            }

            return result;
        }

        private async Task<int> CreateChannel(ChannelRequestModel channelRequest)
        {
            var createRequestRresponse = await _httpInvoker.InvokeAsync(async () =>
            {
                var client = new RestClient(_ChannelApiSettings.Value.BaseUrl);
                var request = new RestRequest(_ChannelApiSettings.Value.CreateUrl, Method.POST);
                request.AddJsonBody(new
                {
                    ChannelId = 1,
                    Name = channelRequest.Name,
                    ImageUrl = channelRequest.ImageUrl,
                    IsActive = true,
                    IsDeleted = false
                });
                //throw new InvalidOperationException("There was an error with Channel service");
                return await client.ExecuteTaskAsync(request);
            });

            return JsonConvert.DeserializeObject<int>(createRequestRresponse.Content);
        }

        private async Task<int> MediaOfChannel()
        {
            var createRequestRresponse = await _httpInvoker.InvokeAsync(async () =>
            {
                var client = new RestClient(_ChannelApiSettings.Value.BaseUrl);
                var request = new RestRequest(_ChannelApiSettings.Value.CreateUrl, Method.POST);
                request.AddJsonBody(new
                {
                    ChannelId = 1,
                    Name = "123",
                    ImageUrl = "123",
                    IsActive = true,
                    IsDeleted = false
                });
                //throw new InvalidOperationException("There was an error with Channel service");
                return await client.ExecuteTaskAsync(request);
            });

            return JsonConvert.DeserializeObject<int>(createRequestRresponse.Content);
        }

    }
}