﻿using System;
using Infrastructure.EventBus.Events;

namespace WebSite.Application.IntegrationEvents.Events
{
    public class ChannelUpdatedIntegrationEvent : IntegrationEvent
    {
        public ChannelUpdatedIntegrationEvent(int ChannelId, string _Name, string _ImageUrl, bool _IsActive, bool _IsDeleted)
        {
            ChannelId = ChannelId;
            Name = _Name;
            ImageUrl = _ImageUrl;
            IsActive = _IsActive;
            IsDeleted = _IsDeleted;
        }

        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
