﻿using System;
using Domain.Channel.Model;
using Domain.SharedKernel.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Channel.Persistence
{
    /// <summary>
    /// This context is only to creates and runs the migrations. Just to example purposes and, avoids that you have to deal running scripts before to execute the solution.
    /// You must use <see cref="IChannelContext"/>
    /// </summary>
    [Obsolete("This context is only to creates and runs the migrations. Just to example purposes and, avoids that you have to deal running scripts before to execute the solution.")]
    public class ChannelMigrationContext : DbContext
    {
        public ChannelMigrationContext(DbContextOptions<ChannelMigrationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ChannelEntityTypeConfiguration());
        }

        public DbSet<ChannelModel> Channels { get; set; }
    }

    public class UserContextDesignFactory : IDesignTimeDbContextFactory<ChannelMigrationContext>
    {
        public ChannelMigrationContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ChannelMigrationContext>()
                .UseSqlServer("Server=.;Initial Catalog=Channeldb;Integrated Security=true");

            return new ChannelMigrationContext(optionsBuilder.Options);
        }
    }

    internal class ChannelEntityTypeConfiguration : IEntityTypeConfiguration<ChannelModel>
    {
        public void Configure(EntityTypeBuilder<ChannelModel> builder)
        {
            builder.ToTable("Channels");

            builder.HasKey(o => o.ChannelId);

            builder.Property(x => x.ChannelId)
                .IsRequired();

            builder.Property(x => x.ChannelId)
                .IsRequired();

            builder.Property(x => x.ImageUrl)
                .IsRequired();

            builder.Property(x => x.IsActive)
                .IsRequired();

            builder.Property(x => x.IsDeleted)
                .IsRequired();

            builder.Property(x => x.Name)
                .IsRequired();
        }
    }
}
