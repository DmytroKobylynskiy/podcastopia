﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.DDD;

namespace Domain.SharedKernel.Model
{
    public class ChannelStatus : Enumeration
    {
        public static ChannelStatus Created = new ChannelStatus(0, nameof(Created));
        public static ChannelStatus Accepted = new ChannelStatus(1, nameof(Accepted));
        public static ChannelStatus Cancelled = new ChannelStatus(2, nameof(Cancelled));
        public static ChannelStatus OnTheWay = new ChannelStatus(3, nameof(OnTheWay));
        public static ChannelStatus InCourse = new ChannelStatus(4, nameof(InCourse));
        public static ChannelStatus Finished = new ChannelStatus(5, nameof(Finished));

        protected ChannelStatus() { }

        public ChannelStatus(int id, string name)
            : base(id, name)
        {
        }

        public static IEnumerable<ChannelStatus> List()
        {
            return new[] { Created, Accepted, Cancelled, OnTheWay, InCourse, Finished };
        }

        public static ChannelStatus FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new ArgumentException($"Possible values for ChannelStatus: {string.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static ChannelStatus From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new ArgumentException($"Possible values for ChannelStatus: {string.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}
