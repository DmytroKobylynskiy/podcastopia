﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebSite.Models;

namespace WebSite.Infrastructure.Repository
{
    public interface IReportingRepository : IDisposable
    {
        Channel GetChannel(int ChannelId);

        void AddChannel(Channel Channel);

        void UpdateChannel(Channel Channel);

        Task AddChannelAsync(Channel Channel);

        Task UpdateChannelAsync(Channel Channel);

        Task<IList<Channel>> GetChannelsAsync();

        Task<Channel> GetChannelAsync(int ChannelId);
        
        Feed GetFeed(int Feed_Id);

        void AddFeed(Feed feed);

        void UpdateFeed(Feed feed);

        Task AddFeedAsync(Feed feed);

        Task UpdateFeedAsync(Feed feed);

        Task<IList<Feed>> GetFeedsAsync();

        Task<Feed> GetFeedAsync(int Feed_Id);
    }
}