using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.ComponentModel.DataAnnotations.Schema;
using Infrastructure.DDD;
using System;

namespace Feed.API.Application.Model{
    public class Item_JSON : Entity, IAggregateRoot{
        [Key]
        public int Json_id{get;set;}

        public int Id { get; set; }
        public string Json_data { get; set; }

        public Item_JSON(int _json_id, int _id, string _json_data){
            Json_id = _json_id;
            Id = _id;
            Json_data = _json_data;
        }

        public Item_JSON(int _id, string _json_data){
            Id = _id;
            Json_data = _json_data;
        }
    }
}