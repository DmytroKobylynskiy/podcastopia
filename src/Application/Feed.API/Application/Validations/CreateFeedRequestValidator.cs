﻿using Feed.API.Application.Model;
using FluentValidation;

namespace Feed.API.Application.Validations
{
    public class CreateFeedRequestValidator : AbstractValidator<CreateFeedRequest>
    {
        public CreateFeedRequestValidator()
        {
            RuleFor(request => request.Channel_Id).NotEmpty().WithMessage("Channel id is required.");
        }
    }
}
