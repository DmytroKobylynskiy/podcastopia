﻿using Infrastructure.EventBus.Events;
using System;
using MediatR;

namespace Domain.Feed.Events
{
    public class FeedCreatedDomainEvent : INotification
    {
        public FeedCreatedDomainEvent(int feedId, string feed_URL, string feed_Description, string name, int channelId, bool isDeleted, bool isActive)
        {
            Feed_Id = feedId;
            Feed_URL = feed_URL;
            Feed_Description = feed_Description;
            Name = name;
            Channel_Id = channelId;
            IsActive = isActive;
            IsDeleted = isDeleted;
        }

        public int Feed_Id;
        public string Feed_URL;
        
        public string Feed_Description;

        public string Name;
        
        public int Channel_Id;

        public bool IsDeleted;

        public bool IsActive;
    }
}
