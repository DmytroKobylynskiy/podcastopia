﻿using System;
using System.Threading.Tasks;
using Infrastructure.EventBus.Abstractions;
using WebSite.Application.IntegrationEvents.Events;
using WebSite.Infrastructure.Repository;
using WebSite.Models;

namespace WebSite.Application.IntegrationEvents.Handlers
{
    public class FeedUpdatedIntegrationEventHandler : IIntegrationEventHandler<FeedUpdatedIntegrationEvent>
    {
        private readonly IReportingRepository _reportingRepository;

        public FeedUpdatedIntegrationEventHandler(IReportingRepository reportingRepository)
        {
            _reportingRepository = reportingRepository ?? throw new ArgumentNullException(nameof(reportingRepository));
        }

        public async Task Handle(FeedUpdatedIntegrationEvent @event)
        {
            Feed feed = await _reportingRepository.GetFeedAsync(@event.Feed_Id);

            // we throw an exception in order to don't send the Acknowledgement to the service bus, probably the consumer read the 
            // this message before that the created one.

            feed.Feed_Id = @event.Feed_Id;
            feed.Feed_URL = @event.Feed_URL;
            feed.Feed_Description = @event.Feed_Description;
            feed.Channel_Id = @event.Channel_Id;

            try
            {
                await _reportingRepository.UpdateFeedAsync(feed);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Error trying to create/update the Feed: {@event.Feed_Id}", ex);
            }
            finally
            {
                _reportingRepository.Dispose();
            }
        }
    }
}
