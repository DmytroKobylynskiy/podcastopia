﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Infrastructure.Persistence;
using WebSite.Models;
using Microsoft.EntityFrameworkCore;

namespace WebSite.Infrastructure.Repository
{
    public class ReportingRepository : IReportingRepository
    {
        private readonly ReportingContext _reportingContext;

        public ReportingRepository(ReportingContext reportingContext)
        {
            _reportingContext = reportingContext ?? throw new ArgumentNullException(nameof(reportingContext));
        }

        public async Task AddChannelAsync(Channel Channel)
        {
            _reportingContext.Channels.Add(Channel);
            await _reportingContext.SaveChangesAsync();
        }

        public void AddChannel(Channel Channel)
        {
            _reportingContext.Channels.Add(Channel);
            _reportingContext.SaveChanges();
        }

        public void UpdateChannel(Channel Channel)
        {
            _reportingContext.Attach(Channel);
            _reportingContext.SaveChanges();
        }

        public async Task UpdateChannelAsync(Channel Channel)
        {
            _reportingContext.Attach(Channel);
            await _reportingContext.SaveChangesAsync();
        }

        public async Task<IList<Channel>> GetChannelsAsync()
        {
            return await _reportingContext.Channels.ToListAsync();
        }

        public async Task<Channel> GetChannelAsync(int ChannelId)
        {
            return await _reportingContext.Channels.SingleOrDefaultAsync(x => x.ChannelId == ChannelId);
        }

        public Channel GetChannel(int ChannelId)
        {
            return _reportingContext.Channels.SingleOrDefault(x => x.ChannelId == ChannelId);
        }

        public async Task AddFeedAsync(Feed feed)
        {
            _reportingContext.Feeds.Add(feed);
            await _reportingContext.SaveChangesAsync();
        }

        public void AddFeed(Feed feed)
        {
            _reportingContext.Feeds.Add(feed);
            _reportingContext.SaveChanges();
        }

        public void UpdateFeed(Feed feed)
        {
            _reportingContext.Attach(feed);
            _reportingContext.SaveChanges();
        }

        public async Task UpdateFeedAsync(Feed feed)
        {
            _reportingContext.Attach(feed);
            await _reportingContext.SaveChangesAsync();
        }

        public async Task<IList<Feed>> GetFeedsAsync()
        {
            return await _reportingContext.Feeds.ToListAsync();
        }

        public async Task<Feed> GetFeedAsync(int feedId)
        {
            return await _reportingContext.Feeds.SingleOrDefaultAsync(x => x.Feed_Id == feedId);
        }

        public Feed GetFeed(int feedId)
        {
            return _reportingContext.Feeds.SingleOrDefault(x => x.Feed_Id == feedId);
        }
        public void Dispose()
        {
            _reportingContext?.Dispose();
        }
    }
}