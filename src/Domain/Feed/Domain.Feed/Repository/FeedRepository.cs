﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Feed.Model;
using Domain.Feed.Persistence;
using Infrastructure.DDD;

namespace Domain.Feed.Repository
{ 
    public class FeedRepository : IFeedRepository
    {
        private readonly IFeedContext _context;

        public FeedRepository(IFeedContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<ModelFeed> GetFeedAsync(int id)
        {
            return await _context.QuerySingleAsync<ModelFeed>("Select * from Feeds where Feed_Id = @id", new { id = id });
        }

        public async Task<ModelFeed> GetFeedByChannelAsync(int Channel_Id)
        {
            return await _context.QuerySingleAsync<ModelFeed>(
                "Select * " +
                "From Feeds  " +
                "Where Channel_Id = @Channel_Id",
                new { Channel_Id = Channel_Id });
        }

        public async Task<Item> GetItemByFeedIdAsync(int _Feed_Id)
        {
            return await _context.QuerySingleAsync<Item>(
                "Select * " +
                "From Items  " +
                "Where Feed_Id = @Feed_Id",
                new { Feed_Id = _Feed_Id });
        }

        public async Task<IEnumerable<ModelFeed>> GetFeedsAsync()
        {
            return await _context.QueryAsync<ModelFeed>(
                "Select * " +
                "From Feeds");
        }

        public async Task<int> AddFeedAsync(ModelFeed feed)
        {
            var _feed = await GetFeedByChannelAsync(feed.Channel_Id);
            if(_feed!=null){
                if(_feed.Feed_URL!=feed.Feed_URL){
                    return await _context.ExecuteAsync(
                        feed,
                        "Insert Into Feeds(Feed_URL, Feed_Description, Channel_Id, Name, IsDeleted, IsActive) Values(+"+"'"+feed._Feed_URL+"'"+","+"'"+feed._Feed_Description+"'"+
                ","+feed._Channel_Id+","+"'"+feed._Name+"'"+","+0+","+ 1+")");
                }else return 0;
            }else return await _context.ExecuteAsync(
                        feed,
                        "Insert Into Feeds(Feed_URL, Feed_Description, Channel_Id, Name, IsDeleted, IsActive) Values(+"+"'"+feed._Feed_URL+"'"+","+"'"+feed._Feed_Description+
                "'"+","+feed._Channel_Id+","+"'"+feed._Name+"'"+","+0+","+ 1+")");
        }

        public async Task<ModelFeed> DeleteFeedAsync(int id)
        {
            var _feed = await GetFeedAsync(id);
            if(_feed!=null){
                return await _context.QuerySingleAsync<ModelFeed>(
                    "Update Feeds set IsDeleted = @_IsDeleted where Feed_Id = @feedId",
                    new
                    {
                        feedId = id,
                        _IsDeleted = true
                    });
            }else return null;
        }

        public async Task<ModelFeed> UpdateChannelIdOfFeedAsync(int _feedId, int _channelId)
        {

            var _feed = await GetFeedByChannelAsync(_feedId);
            if(_feed!=null){
                //if(_feed!=feed){
                    return await _context.QuerySingleAsync<ModelFeed>(
                        "Update Feeds set Channel_Id=@channelId where Feed_Id = @feedId",
                        new
                        {
                            channelId = _channelId,
                            feedId = _feedId
                        });
                //}else return feed;
            }else return null;
        }

        public async Task<ModelFeed> UpdateFeedAsync(ModelFeed feed)
        {

            var _feed = await GetFeedByChannelAsync(feed.Feed_Id);
            if(_feed!=null){
                //if(_feed!=feed){
                    return await _context.QuerySingleAsync<ModelFeed>(
                        "Update Feeds set Channel_Id=@channelId, Feed_URL=@feedUrl, Name=@name, Feed_Description = @description where Feed_Id = @feedId",
                        new
                        {
                            channelId = feed.Channel_Id,
                            feedUrl = feed.Feed_URL,
                            name = feed.Name,
                            description = feed.Feed_Description,
                            feedId = feed.Feed_Id
                        });
                //}else return feed;
            }else return null;
        }

        public async Task AddFeedsAsync(List<ModelFeed> feeds)
        {
            foreach(ModelFeed feed in feeds){
                await _context.ExecuteAsync(
                feed,
                "Insert Into Feeds(Feed_URL, Feed_Description, Channel_Id, Name, IsDeleted, IsActive) Values(+"+feed._Feed_URL+","+feed._Feed_Description+
                ","+feed._Channel_Id+","+feed._Name+","+","+false+","+ true+")");
            }
        }


        public async Task<int> AddItemAsync(Item item)
        {
            /*var _item = await GetItemByFeedIdAsync(item.Feed_Id);
            if(_item!=null){
                if(_item.Feed_Id != item.Feed_Id&&_item.Link!=item.Link){
                }
            }*/
            return await _context.ExecuteAsync(
                item,
                "Insert Into Items(Feed_Id, Guid, Length, Link, Pub_Date, Type) Values(@Feed_Id, @Guid, @Length, @Link, @Pub_Date, @Type)",
                new
                {
                    item.Feed_Id,
                    item.Guid,
                    item.Length,
                    item.Link,
                    item.Pub_Date,
                    item.Type
                });
        }

        public async Task<int> AddItemJsonAsync(Item_JSON item)
        {
            return await _context.ExecuteAsync(
                item,
                "Insert Into Items_JSON(Json_id, Id, Json_data) Values(@Json_id, @Id, @Json_data)",
                new
                {
                    item.Id,
                    item.Json_id,
                    item.Json_data
                });
        }


        public void Dispose()
        {
            _context?.Dispose();
        }
/*
        public List<int[]> FeedsIdOfChannel(int[] id)
        {
            List<int[]> feedsId = new List<int[]>();
            for (int i = 0; i < id.Length; i++)
            {
                var items = from feed in _context.Feeds
                            where feed.Channel_Id == id[i]
                            select feed.Feed_Id;
                feedsId.Add(items.ToArray());
            }
            return feedsId;
        }
        */
        public async Task<IEnumerable<FeedId>> FeedsIdOfChannel(int _id)
        {
            var list = await _context.QueryAsync<FeedId>(
                "Select Feed_Id " +
                "From Feeds  " +
                "Where Channel_Id = @id",
                new { id = _id });
            return list;
        }


     

        public async Task AddItemsJson(List<Item_JSON> items_json)
        {
            var sql = "Insert Into Items_JSON(Id, Json_data) Values";
            foreach (Item_JSON item in items_json)
            {
                sql = sql + "("+item.Id+","+"'"+item.Json_data+"'),";
            }
            sql = sql.Remove(sql.LastIndexOf(",")) + ";";
            await _context.QuerySingleAsync<Item_JSON>(
                 sql);
        }

        public async Task AddItemsAsync(List<Item> items)
        {
            var sql = "Insert Into Items(Feed_Id, Guid, Length, Link, Type, Pub_Date) Values";
            foreach (Item item in items)
            {
                sql = sql + 
                "("
                +item.Feed_Id+","+"'"+item.Guid +"'"+","+ item.Length+ ","+"'"+item.Link+"'"+","+"'"+item.Type+"'"+","
                +"'"+item.Pub_Date+"'"+"),";
            }
            sql = sql.Remove(sql.LastIndexOf(",")) + ";";
            await _context.QuerySingleAsync<Item>(
                 sql);
        }

    }

    public class FeedId : Entity, IAggregateRoot
    {
        public int Feed_Id { get; set; }
    }
}