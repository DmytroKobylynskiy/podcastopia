﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Domain.User.Model;
using Domain.User.Repository;
using Microsoft.AspNetCore.Mvc;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserRepository _userRepository;

        public HomeController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IActionResult Index()
        {
            // users test
            //var users = _userRepository.GetUsersAsync().Result;
            //var user = users[0];
            //user.ChangePaymentMethod(PaymentMethod.Cash);
            //_userRepository.Update(user);
            //var result =_userRepository.UnitOfWork.SaveChangesAsync().Result;
            //var paymentMethod = user.PaymentMethod;

            // drivers test
            //var drivers = _driverRepository.GetDriversAsync().Result;
            //var driver = drivers[0];
            //driver.AddVehicle("TWN 741", "Lexus", "2018", VehicleType.Car);
            //_driverRepository.Update(driver);
            //var result = _driverRepository.UnitOfWork.SaveChangesAsync().Result;
            //var currentVehicle = driver.CurrentVehicle;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
